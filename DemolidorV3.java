package fatec2018;
import robocode.*;
import java.awt.geom.*;
import java.awt.*;

public class Demolidor  extends TeamRobot
{

    Point2D direcao;  // Vetor normalizado com a direção de movimento do tanque
    Point2D canto;    // Vetor do tanque até o canto mais próximo a que ele se aproxima
    double distancia; // Distância até as paredes

    final double limite = 250; 
	boolean andarFrente;
	boolean inWall;

	public void run() {

boolean angulo =true;

setGunColor(Color.black);
setBodyColor(Color.red);
setRadarColor(Color.white);
setBulletColor(Color.white);
setScanColor(Color.yellow);
	
// Robot main loop
		while(true) {
			calculaDistancia();
			if(angulo == true){
			for (int voltasEsq=0;voltasEsq < 4 ; voltasEsq ++) {
				
			setTurnRadarRight(360);
			setAhead(100);
			setTurnRight(90);
			setTurnGunLeft(90);
			

		}
		for (int voltasDir = 4; voltasDir >0 ;voltasDir-- ) {
			setTurnRadarRight(360);
			setAhead(100);
			setTurnLeft(90);
			setTurnGunRight(180);
			
			}
			angulo = false;
		}
		else{
			for (int voltasEsq45=0;voltasEsq45 < 4 ; voltasEsq45 ++) {
				
			setTurnRadarRight(360);
			setAhead(100);
			setTurnRight(90);
			setTurnGunLeft(90);
			execute();

		}
		for (int voltasDir45 = 8; voltasDir45 >0 ;voltasDir45-- ) {
			setTurnRadarRight(360);
			setAhead(50);
			setTurnLeft(45);
			setTurnGunRight(180);
			
			}
			angulo = true;
		}
	}
}
	public void calculaDistancia() {

        // Pega a altura e largura do campo de batalha e posição x,y do tanque
        double h = getBattleFieldHeight(); // Altura
        double w = getBattleFieldWidth();  // Largura
        double x = getX();
        double y = getY();

        // Pega a direção em que o tank se move e a sua posição atual (x, y) no campo de batalha
        double ang = getHeading(); // O ângulo está em graus, variando entre 0 (apontando pra cima) e 359) no sentido horário

        // Calcula os vetor normal de direção do tanque
        double dx = Math.sin(Math.toRadians(ang));
        double dy = Math.cos(Math.toRadians(ang));
        direcao = new Point2D.Double(dx, dy);

        // Calcula o vetor do tanque em direção ao canto mais próximo da direção e sentido que ele segue
        dx = (direcao.getX() > 0) ? w - x : -x;
        dy = (direcao.getY() > 0) ? h - y : -y;
        canto = new Point2D.Double(dx, dy);

        // Calcula os angulos entre o vetor de direcao e os vetores dos os eixos x e y
        double angX = Math.acos(Math.abs(direcao.getX()));
        double angY = Math.acos(Math.abs(direcao.getY()));

        // A distância é o cateto adjascente do menor ângulo
        if(angY < angX)
            distancia = Math.abs(canto.getY() / Math.cos(angY));
        else
            distancia = Math.abs(canto.getX() / Math.cos(angX));
    }   

    public void onPaint(Graphics2D g) {
        // Desenha a linha até a parede em amarelo se maior do que o limite, e em vermelho se menor do que o limite
        if(distancia <= limite)
            g.setColor(java.awt.Color.RED);
        else
            g.setColor(java.awt.Color.YELLOW);
        g.drawLine((int) getX(), (int) getY(), (int) (getX() + (distancia * direcao.getX())), (int) (getY() + (distancia * direcao.getY())));

        // Desenha o valor da distância em branco
        g.setColor(java.awt.Color.WHITE);
        g.drawString("Distancia: " + distancia, 10, 10);

        // Desenha as componentes do vetor do canto tracejados em branco
        Stroke pontilhado = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{10}, 0);
        g.setStroke(pontilhado);
        g.setColor(java.awt.Color.WHITE);
        g.drawLine((int) getX(), (int) getY(), (int) (getX() + canto.getX()), (int) getY()); // Componente em X
        g.drawLine((int) getX(), (int) getY(), (int) getX(), (int) (getY() + canto.getY())); // Componente em Y
     }
	public void onScannedRobot(ScannedRobotEvent e) {
if(isTeammate(e.getName())){
		return;
		}

 double distancia = e.getDistance();
 double angulo = e.getBearing();
 turnRight(angulo);
 ahead(distancia/2);
  if(e.getDistance() < 150) {
 fire(1);
 }
	}
		public void onHitRobot(HitRobotEvent e){
double mira= normalRelativeAngle(e.getBearing() +(getHeading() - getRadarHeading()));		
turnRight(mira);
		fire(2);
		back(100);
		setTurnLeft(180);
		execute();
	}
public double normalRelativeAngle(double angle){
	if(angle > -180 && angle <=180){
		return angle;
	}
	double fixedAngle = angle;
	while(fixedAngle <= -180){
		fixedAngle +=360;
	}
	while(fixedAngle >180){
		fixedAngle -=360;
	}
	return fixedAngle;
}
	/**
	 * onHitByBullet: What to do when you're hit by a bullet
	 */
	public void onHitByBullet(HitByBulletEvent e) {
		// Replace the next line with any behavior you would like
		setTurnLeft(0);
		setAhead(200);
		double angulo= e.getBearing();
		turnGunRight(angulo);
		fire(1);
		execute();
	}
	


	public void onHitWall() {
	
		setTurnRight(180);
		setAhead(200);
		execute();
	}	
	
}

